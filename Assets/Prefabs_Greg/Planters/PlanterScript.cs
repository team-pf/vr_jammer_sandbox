﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanterScript : MonoBehaviour {

    enum PlanterState { waitingSeed, waitingWater, waitingGrowth, waitingHarvest };
    enum PlantType { carrot };
    const float switchPeriod = 5.0f;

    Transform    carrot;
    Transform    turnip;
    Transform    wateringSign;
    Transform    doneSign;
    Transform    earth;
    Transform    growingPlant;
    float        timeUntilSwitch;
    PlanterState planterState;
    PlantType    plantType;

    private AudioSource source;

    public Material planterEarthDry;
    public Material planterEarthWet;
    public AudioClip doneSound;

    // Use this for initialization
    void Start () {
        planterState = PlanterState.waitingSeed;
        timeUntilSwitch = switchPeriod;
        foreach (Transform t in transform)
        {
            if (t.name == "Plants")
            {
                foreach (Transform plants in t)
                {
                    if (plants.name == "Carrot")
                    {
                        carrot = plants;
                    }
                    else if (plants.name == "Turnip")
                    {
                        turnip = plants;
                    }
                }
            }
            else if (t.name == "Icons")
            {
                foreach (Transform icons in t)
                {
                    if (icons.name == "Water")
                    {
                        wateringSign = icons;
                    }
                    else if (icons.name == "Done")
                    {
                        doneSign = icons;
                    }
                }
            }
            else if (t.name == "Earth")
            {
                earth = t;
            }
        }
        source = GetComponent<AudioSource>();
    }
	
    void OnSeed()
    {
        planterState = PlanterState.waitingWater;
        wateringSign.gameObject.SetActive(true);
        growingPlant = turnip;
    }

    void OnWater()
    {
        planterState = PlanterState.waitingGrowth;
        wateringSign.gameObject.SetActive(false);

        Material[] mats = earth.gameObject.GetComponent<Renderer>().materials;
        mats[0] = planterEarthWet;
        earth.gameObject.GetComponent<Renderer>().materials = mats;

        growingPlant.gameObject.SetActive(true);
    }

    void OnGrowth()
    {
        planterState = PlanterState.waitingHarvest;
        doneSign.gameObject.SetActive(true);

        Material[] mats = earth.GetComponent<Renderer>().materials;
        mats[0] = planterEarthDry;
        earth.GetComponent<Renderer>().materials = mats;

        growingPlant.localScale = Vector3.one;

        source.PlayOneShot(doneSound);
    }

    void OnHarvest()
    {
        planterState = PlanterState.waitingSeed;
        doneSign.gameObject.SetActive(false);
        growingPlant.gameObject.SetActive(false);
    }

    void OnSwitch()
    {
        switch(planterState)
        {
            case PlanterState.waitingSeed:
                OnSeed();
                return;
            case PlanterState.waitingWater:
                OnWater();
                return;
            case PlanterState.waitingGrowth:
                OnGrowth();
                return;
            case PlanterState.waitingHarvest:
                OnHarvest();
                return;
            default:
                Debug.LogError("Unknown planter state !");
                break;
        }
    }

    void UpdateGrowth()
    {
        if (planterState == PlanterState.waitingGrowth)
        {
            float scale = (switchPeriod - timeUntilSwitch) / switchPeriod;
            growingPlant.localScale = Vector3.one * scale;
        }
    }

    // Update is called once per frame
    void Update () {
        float deltaTime = Time.deltaTime;
        if (timeUntilSwitch < deltaTime)
        {
            OnSwitch();
            timeUntilSwitch = switchPeriod;
        }
        else
        {
            timeUntilSwitch -= deltaTime;
        }
        UpdateGrowth();
    }
}
