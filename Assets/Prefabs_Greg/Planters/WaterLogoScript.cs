﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterLogoScript : MonoBehaviour {

    const float rotationSpeed = 50.0f;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        float deltaTime = Time.deltaTime;

        Vector3 rotation = Vector3.up * (rotationSpeed * deltaTime);

        transform.Rotate(rotation);
    }
}
