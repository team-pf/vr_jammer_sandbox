﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PigScript : MonoBehaviour {

    const float rotationSpeed = 10.0f;
    const float walkingSpeed = 1.0f;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        float deltaTime = Time.deltaTime;
        Vector3 rotation = Vector3.up * (rotationSpeed * deltaTime);
        transform.Rotate(rotation);
        Vector3 walkingForward = Vector3.forward * (walkingSpeed * deltaTime);
        transform.Translate(walkingForward);
	}
}
